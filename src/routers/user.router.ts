import { Router, Request } from "express";
import { IUser } from "../user.Interface";
import * as dbUtils from "../db.js";
import { pathDB, NODE_ENV, userController } from "../index.js";


const errorObj = (e: Error, req: Request) => {
  return {
    status: 404,
    reqID: req.id,
    message: e.message,
    stack: NODE_ENV == "development" ? e.stack : undefined,
  };
};

export const userRouter = Router();

//-----------------------------CREATE-----------------------------
userRouter.post("/", (req, res, next) => {
  try {
    const user = userController.createUser(req);
    dbUtils.saveToDB(pathDB, req.users);
    res.status(200).json({
      status: res.status,
      message: "Successfully added",
      user,
    });
  } catch (e) {
    // res.status(404).send((e as Error).message);
    next(errorObj(e as Error, req));
  }
});

//-----------------------------UPDATE-----------------------------
userRouter.put("/:userID", (req, res, next) => {
  try {
    const user = userController.updateUser(req);
    dbUtils.saveToDB(pathDB, req.users);
    res.status(200).json({
      status: res.status,
      message: "Successfully updated",
      user,
    });
  } catch (e) {
    // res.status(404).send((e as Error).message);
    next(errorObj(e as Error, req));
  }
});

//-----------------------------DELETE-----------------------------
userRouter.delete("/:userID", (req, res, next) => {
  try {
    const user = userController.deleteUser(req);
    dbUtils.saveToDB(pathDB, req.users);
    res.status(200).json({
      status: res.status,
      message: "Successfully deleted",
      user,
    });
  } catch (e) {
    // res.status(404).send((e as Error).message);
    next(errorObj(e as Error, req));
  }
});

//-----------------------------READ-ID-----------------------------
userRouter.get("/:userID", (req, res, next) => {
  try {
    const user = userController.getUser(req);
    res.status(200).json(user);
  } catch (e) {
    // res.status(404).send((e as Error).message);
    next(errorObj(e as Error, req));
  }
});

//-----------------------------READ-ALL-----------------------------
userRouter.get("/", (req, res, next) => {
  const userArr: IUser[] = userController.getAllUsers(req);
  try {
    res.status(200).json(userArr);
  } catch (e) {
    // res.status(404).send((e as Error).message);
    next(errorObj(e as Error, req));
  }
});
