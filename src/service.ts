import { IUser } from "./user.Interface";
import { Request } from "express";
export default class UserController {

  getAllUsers(req: Request): IUser[] {
    return req.users;
  }

  getUser(req: Request): IUser {
    const arr: IUser[] = req.users;
    const id = req.params.userID;
    let index = arr.findIndex((d) => d.id === id);
    if (index >= 0) {
      return arr[index];
    } else {
      throw new Error(`Error: ${id} not exist`);
    }
  }

  generateID() {
    return Math.random().toString(36).substring(2);
  }

  createUser(req: Request) {
    const user: IUser = { id: this.generateID(), ...req.body };
    req.users.push(user);
    return user;
  }

  updateUser(req: Request) {
    const arr: IUser[] = req.users;
    const user: IUser = {
      id: req.params.userID,
      ...req.body,
    };
    let index = arr.findIndex((d) => d.id === user.id);
    if (index >= 0) {
      arr[index] = user;
    } else {
      throw new Error(`Error: ${user.id} not exist`);
    }
    return arr[index];
  }

  deleteUser(req: Request) {
    const arr: IUser[] = req.users;
    const id = req.params.userID;
    let index = arr.findIndex((d) => d.id === id);
    if (index >= 0) {
      arr.splice(index, 1);
    } else {
      throw new Error(`Error: ${id} not exist`);
    }
  }
}
