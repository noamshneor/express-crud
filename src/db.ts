import { IError, IUser } from "./user.Interface";
import fs from "fs/promises";
import { Request } from "express";

// -------------------------- DB ---------------------------

export async function getUsers(pathDB: string) {
  try {
    const file = JSON.parse(await fs.readFile(pathDB, "utf-8"));
    return file;
  } catch (error) {
    fs.writeFile(pathDB, JSON.stringify([]), "utf-8"); // if file not exists
    return [];
  }
}

export async function checkLOG(pathLOG: string) {
  try {
    const file = await fs.readFile(pathLOG, "utf-8");
    return file;
  } catch (error) {
    fs.writeFile(pathLOG, "LOG FILE", "utf-8"); // if file not exists
    return "LOG FILE";
  }
}

export async function saveToDB(pathDB: string, users: IUser[]) {
  fs.writeFile(pathDB, JSON.stringify(users), "utf-8");
}

export async function addToLOG(req: Request, pathLOG: string) {
  const newStr = `${await fs.readFile(pathLOG, "utf-8")}
  ${req.id} --> ${req.method} :: ${req.originalUrl} >> ${Date.now()}`;
  fs.writeFile(pathLOG, newStr, "utf-8");
}

export async function addToErrorLOG(err: IError, pathErrorLOG: string) {
  const newStr = `${await fs.readFile(pathErrorLOG, "utf-8")}
  ${err.reqID} --> ${err.status} :: ${err.message}
  ${err.stack ? err.stack : ""} 
  >> ${Date.now()}`;
  fs.writeFile(pathErrorLOG, newStr, "utf-8");
}
