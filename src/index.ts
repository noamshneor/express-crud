import express from "express";
import log from "@ajar/marker";
import { userRouter } from "./routers/user.router.js";
import {
  logger,
  errorLogger,
  getData,
  getLogs,
  setID,
  urlNotFound,
} from "./middleware.js";
export const pathDB = "./users.json";
export const pathLOG = "./users.log";
export const pathErrorLOG = "./error.log";

import UserController from "./service.js";
export const userController = new UserController();

export const { PORT = 3030, HOST = "localhost", NODE_ENV } = process.env;

const app = express();

(async () => {
  app.use(setID);
  app.use(getData);
  app.use(getLogs);

  app.use(express.json());

  app.use("/api/user", logger, userRouter);

  app.use(urlNotFound);
  app.use(errorLogger);

  app.listen(Number(PORT), HOST as string, () => {
    log.magenta(`🌎  listening on`, `http://${HOST}:${PORT}`);
  });
})();

//------------------------------------------
//         Express CRUD - json file
//------------------------------------------
/* challenge instructions

Create a new TS based express rest API
It needs to implement a users router and will handle incoming requests 
to Create, Update and Delete a user
It will also allow us to Read a single user and return a list of all existing users

Our DB will be a JSON file
We will also implement a log plain text file that will log each request made to the server 
in a single line and will include the request http method, the path and a timestamp


*/
