export interface IUser {
  id: string;
  email: string;
  pw: string;
}

export interface IError {
  status: number;
  reqID: string;
  message: string;
  stack?: string;
}
