import { NextFunction, Request, Response } from "express";
import { pathLOG, pathErrorLOG, pathDB, userController } from "./index.js";
import { addToLOG, addToErrorLOG, getUsers, checkLOG } from "./db.js";
import { IError, IUser } from "./user.Interface";

export async function setID(req: Request, res: Response, next: NextFunction) {
  req.id = userController.generateID();
  next();
}

export async function getData(req: Request, res: Response, next: NextFunction) {
  const users: IUser[] = await getUsers(pathDB);
  req.users = users;
  next();
}

export async function getLogs(req: Request, res: Response, next: NextFunction) {
  await checkLOG(pathLOG);
  await checkLOG(pathErrorLOG);
  next();
}

export function logger(req: Request, res: Response, next: NextFunction) {
  addToLOG(req, pathLOG);
  next();
}

export function errorLogger(
  err: IError,
  req: Request,
  res: Response,
  next: NextFunction
) {
  console.log("error handler --> ", err.message);
  addToErrorLOG(err, pathErrorLOG);
  res.status(err.status).json({
    status: err.status,
    reqID: req.id,
    message: err.message,
    stack: err.stack,
  });
}

export function urlNotFound(req: Request, res: Response, next: NextFunction) {
  const full_url = new URL(req.url, `http://${req.headers.host}`);
  res.status(404).send(` - 404 - url ${req.url} was not found`);
}
